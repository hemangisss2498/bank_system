package com.montran.action;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.montran.form.Record;

public class FileRetrieveAction extends org.apache.struts.action.Action {

	
    /* forward name="success" path="" */
   // private final static String SUCCESS = "success";
    private final static String READ= "read";

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	System.out.println("Retrieving");
    	Connection con=DriverManager.getConnection("jdbc:postgresql://localhost/postgres","postgres", "2498");
		Statement stmt=con.createStatement();
		
		String sql="Select * from file1"; //table 
		
		Statement st=con.createStatement();
		
		ResultSet rs=st.executeQuery(sql);
		
		
		List<Record> recordlist=new ArrayList<Record>();
		while(rs.next())
		{
			
		
			long anumber=rs.getLong(1);
			String fname=rs.getString(2);
			String amount=rs.getString(3);
			String ifsc=rs.getString(4);
			String status=rs.getString(5);
			String reason=rs.getString(6);
			
			Record rec=new Record(anumber, fname, amount, ifsc, status, reason);
			
			recordlist.add(rec);
			System.out.println(recordlist);
			 
	            
		}
		 request.setAttribute("data", recordlist);
		  RequestDispatcher rd = 
		             request.getRequestDispatcher("read.jsp");
		  rd.forward(request, response);
	         
        return mapping.findForward(READ); 
    }
}