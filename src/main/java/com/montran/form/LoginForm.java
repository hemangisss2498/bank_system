package com.montran.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
 
public class LoginForm extends ActionForm {
	private int id;
    public int getId() {
		return id;
	}
    

	public void setId(int id) {
		this.id = id;
	}

	private String userName ;
    private String pass ;
  
 
    public String getUserName() {
        return userName;
    }
 
    public void setUserName(String userName) {
        this.userName = userName;
    }
 
    public String getPassword() {
        return pass;
    }
 
    public void setPassword(String password) {
        this.pass = password;
    }
 
    @Override
	public ActionErrors validate(ActionMapping mapping,HttpServletRequest request)
	 {
    	ActionErrors ae = new ActionErrors();
		if(userName.equals(""))
			ae.add("userName",new ActionMessage("msg"));
		//System.out.println("Action Errors"+ae);
		return ae;
    }
 
}
