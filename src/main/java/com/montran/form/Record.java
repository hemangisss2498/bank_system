package com.montran.form;

import java.util.Date;

import org.apache.struts.action.ActionForm;

public class Record {
	private long anumber;
	private String fname	;
	private String amount;
	private String ifsc	;
	private String status;
	private String reason;
	@Override
	public String toString() {
		return "Record [anumber=" + anumber + ", fname=" + fname + ", amount=" + amount + ", ifsc=" + ifsc + ", status="
				+ status + ", reason=" + reason + "]";
	}
	public Record(long anumber, String fname, String amount, String ifsc, String status, String reason) {
		super();
		this.anumber = anumber;
		this.fname = fname;
		this.amount = amount;
		this.ifsc = ifsc;
		this.status = status;
		this.reason = reason;
	}
	public long getAnumber() {
		return anumber;
	}
	public void setAnumber(long anumber) {
		this.anumber = anumber;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getIfsc() {
		return ifsc;
	}
	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	

}
