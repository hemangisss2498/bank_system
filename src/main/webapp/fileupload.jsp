<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   <%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>File Upload</title>
    </head>
    <body>
    <center>
        <html:errors />
        <h3>Choose File For Uploading</h3>
        <html:form action="fileUploadAction" method="post" enctype="multipart/form-data">
        
        <table bgcolor="#FFEBFF" border="1" align="center">
		
		<tr>
		<th>
            File : <html:file property="file" /></th></tr> <br/>
          <tr> <th> <html:submit /> </th></tr>
          <tr> <th> <a href="http://localhost:8080/BankSSS/fileRetrieveSingleUserAction.do">Read User</a> </th></tr>
          </table>
        </html:form>
        </center>
    </body>
</html>
